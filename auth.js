let jwt = require('jsonwebtoken');
let secret = "e-commerce-API";

//create token
module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

//verify token
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	// console.log(token); //bearer jkhsdjgslkdfh'psuigjkad

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return res.send({auth: "failed"})
			} else {
				next();
			}
		})

	}
}

//decode token
module.exports.decode = (token) => {

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null
			} else {
				return jwt.decode(token, {complete:true}).payload
			}

		})
	}
}