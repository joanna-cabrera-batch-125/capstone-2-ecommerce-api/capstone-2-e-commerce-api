const Product = require('./../Models/Product')


//create a product -admin only
module.exports.createProduct = (reqBody) => {
	//console.log(reqBody)

	let newProduct = new Product({
		productName: reqBody.productName,
		description: reqBody.description,
		Price: reqBody.Price
	})

	return newProduct.save().then( (product, error) => {
		if(error){
			return error
		} else {
			return true
		}
	})
}

//retrieve all products
module.exports.getAll = () => {

	return Product.find().then( result => {
		
		return result
	})
}


//retrieve all active products
module.exports.getAllActive = () => {

	return Product.find({isActive: true}).then( result => {
		
		return result
	})
}

//get single product
module.exports.getSingleProduct = (params) => {
	//console.log(params)

	return Product.findById(params.productId).then( product => {

		return product
	})
}

//update product info - admin only
module.exports.updateProduct = (params, reqBody) => {

	let updatedProduct = {
		productName: reqBody.productName,
		description: reqBody.description,
		Price: reqBody.Price
	}
	return Product.findByIdAndUpdate(params, updatedProduct, {new: true})
	.then((result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}
//archive product - admin only

module.exports.archiveProduct = (params) => {

	let updatedActiveProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params, updatedActiveProduct, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}
//UNARCHIVE product - admin only

module.exports.unarchiveProduct = (params) => {

	let inactiveProduct = {
		isActive: true
	}

	return Product.findByIdAndUpdate(params, inactiveProduct, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}
module.exports.deleteProduct = (params) => {

	return Product.findByIdAndDelete(params)
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}