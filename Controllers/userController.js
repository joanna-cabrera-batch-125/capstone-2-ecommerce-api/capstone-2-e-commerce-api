const Product = require('./../Models/Product');
const User = require("./../Models/User");
const bcrypt = require('bcrypt');
const auth = require('./../auth');

module.exports.checkEmailExists = (reqBody) => {
	// console.log(reqBody)
	//model.method
	return User.find({email: reqBody.email})
	.then( (result) => {
		if(result.length != 0){
			return true
		} else {
			return false
		}
	})
}


module.exports.register = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then( (result, error) =>{
		if(error){
			return error
		} else {
			return result
		}

		// return true
	})
}

//user controller

//to switch user isAdmin false to true
module.exports.setAsAdmin = (params)=> {

	let updatedUserType = {
		isAdmin : true
	}
	
	return User.findByIdAndUpdate(params, updatedUserType, {new: true}).then((result, error) => {
		if (error) {
			return false
		} else {
			return result
		}
	})
}


//to switch user isAdmin false to true
module.exports.unsetAsAdmin = (params)=> {

	let userType = {
		isAdmin : false
	}

	return User.findByIdAndUpdate(params, userType, {new: true}).then((result, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}

module.exports.login = (reqBody) => {
	//model method
	return User.findOne({email: reqBody.email}).then( (result) => {
		console.log(result);

		if(result == null){
			return false
		}else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) //boolean return
			if(isPasswordCorrect === true){
				return {access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}


module.exports.getProfile = (id) => {
	//console.log(data)
	//Model.method
	return User.findById(id).then( result => {

		result.password = "******"
		return result
	})
}
