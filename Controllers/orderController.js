const Order = require('./../Models/Orders')

//create order non-admin only
module.exports.createOrder = (userId, reqBody) => {
	let newOrder = new Order({
		orderOwner:userId,
		purchasedOn:reqBody.purchasedOn,
		totalAmount:reqBody.totalAmount		

	})
		newOrder.orders.push({productId:reqBody.productId, productName:reqBody.productName})
	return newOrder.save().then( (order, error) => {
		if(error){
			return error
		} else {
			return true
		}
	})
}


//retrieve all orders
module.exports.getAllOrder = () => {

	return Order.find().then( result => {
		
		return result
	})
}

 //retrieve User Orders - non-admin only

module.exports.getSingleOrder = (userId) => {
	return Order.findById(orderId).then(user => {
		if(user === null){
			return false;
		} else {
			return user.orders;
		}
	})
}

// // TRY
// module.exports.checkout = (userId, cart) => {
// 	console.log(cart.products[0].productName);

// 	return Order.findById(orderId).then(order => {
// 		if(order === null){
// 			return false;
// 		} else {
// 			orders.push(
// 				{
// 					products: cart.products,
// 					totalAmount: cart.totalAmount
// 				}
// 			);
// 			console.log(user.orders);
// 			return order.save().then((updatedOrder, error) => {
// 				if(error){
// 					return false;
// 				} else {
// 					const currentOrder = updatedUser.orders[updatedUser.orders.length-1];
					
// 					currentOrder.products.forEach(product => {
// 						Product.findById(product.productId).then(foundProduct => {
// 							foundProduct.orders.push({orderId: currentOrder._id})

// 							foundProduct.save()
// 						})
// 					});

// 					return true;
// 				}
// 			})
// 		}
// 	})
// }








