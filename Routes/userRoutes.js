const express = require("express");
const router = express.Router();

//controllers
const userController = require("./../Controllers/userController");

const auth = require('./../auth');


//check if email exists
router.post("/checkEmail", (req, res) => {
	// console.log(req.body)
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

//user registration
router.post("/register", (req, res)=> {

userController.register(req.body).then(result => res.send(result));

})

//user route

//to switch user isAdmin true to false
router.put('/:userId/setAsAdmin', auth.verify,(req,res)=>{
	console.log(req.params.userId)

	let userData = auth.decode(req.headers.authorization).isAdmin
	console.log(userData)
	/*let userType = userData.isAdmin
	console.log(userType)*/
	if (userData === false || userData === null ) {
		console.log("User not authorized")
	}
	else {

	userController.setAsAdmin(req.params.userId).then(result => res.send(result));
	}
})

//to switch user isAdmin false to true
router.put('/:userId/unsetAsAdmin', auth.verify,(req,res)=>{
	console.log(req.params.userId)

	let userData = auth.decode(req.headers.authorization).isAdmin
	console.log(userData)
	// let userType = userData.isAdmin
	// console.log(userType)
	if (userData === false || userData === null ) {
		console.log("User not authorized")
	}
	else {
	userController.unsetAsAdmin(req.params.userId).then(result => res.send(result));
	}

})


//login
router.post("/login", (req, res) => {

	userController.login(req.body).then( result => res.send(result))
})

//getprofile
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	// console.log(userData)

	userController.getProfile(userData.id).then(result => res.send(result))
})
module.exports = router;
