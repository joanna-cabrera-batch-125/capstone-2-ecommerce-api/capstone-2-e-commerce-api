const express = require('express');
const router = express.Router();
let auth = require('./../auth');


const orderController = require('./../Controllers/orderController');

//create order
router.post('/createOrder', (req, res) => {
	let user = auth.decode(req.headers.authorization);
	console.log(user);
	 orderController.createOrder(user.id, req.body).then( result => res.send(result))
})


 //retrieve all orders - admin only

router.get("/orders", auth.verify, (req, res) => {

	orderController.getAllOrder().then( result => res.send(result))
})

//retrieve User Orders - non admin only // try

router.get("/myOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		res.send(false);
	} else {
		userController.getMyOrders(userData.id).then(resultFromController => res.send(resultFromController));
	}
})


// // try
// router.post("/checkout", auth.verify, (req, res) => {
// 	if(auth.decode(req.headers.authorization).isAdmin === true) {
// 		res.send(false);
// 	} else {
// 		const userId = auth.decode(req.headers.authorization).id;
// 		userController.checkout(userId, req.body).then(resultFromController => res.send(resultFromController));
// 	}
// })



module.exports = router;