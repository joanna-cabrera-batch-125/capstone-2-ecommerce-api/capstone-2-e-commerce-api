const express = require('express');
const router = express.Router();
let auth = require('./../auth');

//controllers
const productController = require('./../Controllers/productController');

// create product
router.post('/createProduct', auth.verify, (req, res) => {

	productController.createProduct(req.body).then( result => res.send(result))
})

//retrieve all active products
router.get('/products', (req, res) => {

	productController.getAllActive().then( result => res.send(result));
})


//retrieve all product
router.get("/getAll", auth.verify, (req, res) => {

	productController.getAll().then( result => res.send(result))
})

//update the product -admin only
router.put('/:productId/update', auth.verify, (req, res) => {
	console.log(req.params.productId)

	productController.updateProduct(req.params.productId, req.body).then( result => res.send(result))
})


// ARCHIVE the product - admin only

router.put('/:productId/archive', auth.verify, (req, res) => {

	productController.archiveProduct(req.params.productId).then( result => res.send(result))
})

// UNARCHIVE the product - admin only
router.put('/:productId/unarchive', auth.verify, (req, res) => {

	productController.unarchiveProduct(req.params.productId).then( result => res.send(result))
})
router.delete('/:productId/delete', auth.verify, (req, res) => {
	console.log(req.params.productId)

	productController.deleteProduct(req.params.productId).then( result => res.send(result))
})

module.exports = router;