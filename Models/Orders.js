const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema(
	{
		totalAmount: {
			type: Number
			// required: [true, "total amount is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		orderOwner:{
			type: String,
			required: [true, "orderOwner is required"]
		},
		paymentStatus:{
			type: Boolean,
			default: false
		},
		orders: [
			{
				productId:{
					type: String
				},
				productName:{
					type: String
				}
			}
		]
	}
);

module.exports = mongoose.model("Orders", orderSchema);