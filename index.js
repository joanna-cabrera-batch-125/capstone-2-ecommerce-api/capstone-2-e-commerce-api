const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT ||8000;
const app = express();
const cors = require('cors');

//routes
let userRoutes = require("./Routes/userRoutes");
let productRoutes = require("./Routes/productRoutes");
let orderRoutes = require("./Routes/orderRoutes");


//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//mongoose connection
mongoose.connect("mongodb+srv://JoannaMC:Admin@cluster0.wx5pn.mongodb.net/e-commerce?retryWrites=true&w=majority",
	{useNewUrlParser: true, useUnifiedTopology: true}
).then(()=>console.log(`Connected to Database`))
.catch( (error)=> console.log(error))



// console.log("try");
app.use(`/api/users`, userRoutes);
app.use(`/api/products`, productRoutes);
// console.log("try 2");
app.use(`/api/orders`, orderRoutes);
// console.log("try 3");

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));

